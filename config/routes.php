<?php

return [
    'GET /auth' => 'AuthController::getAuthForm',
    'POST /auth' => 'AuthController::auth',
    'GET /' => 'MainController::index',
    'POST /write-off' => 'MainController::writeOff',
];