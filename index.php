<?php

declare(strict_types=1);

use Src\Application;

include_once 'vendor/autoload.php';

//load environment
$vars = file('.env');
foreach ($vars as $var) {
    putenv(trim($var));
}

try {
    $app = new Application();
    echo $app->get();
} catch (Throwable $e) {
    echo $e->getMessage();
}
