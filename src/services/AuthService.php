<?php

namespace Src\Services;

use Src\Repositories\UserRepository;

class AuthService extends BaseService
{
    /** @var UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        parent::__construct();
    }

    /**
     * @param array $data
     * @throws \Exception
     */
    public function auth(array $data): void
    {
        $this->checkUser($data);

        session_start();
        $_SESSION['email'] = $data['email'];
        session_write_close();
        $server = $_SERVER['HTTP_HOST'];
        header("Location: http://$server");
    }

    /**
     * @param array $data
     * @return void
     * @throws \Exception
     */
    private function checkUser(array $data): void
    {
        $user = $this->userRepository->getUserByEmail($data['email']);
        $this->checkPassword($user->getPassword(), $data['password']);
    }

    /**
     * @param string|null $password
     * @param string $requestPassword
     * @throws \Exception
     */
    private function checkPassword(?string $password, string $requestPassword): void
    {
        if (!$password || !password_verify($requestPassword, $password)) {
            throw new \Exception('Неудачная авторизация');
        }
    }
}