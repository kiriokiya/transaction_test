<?php

namespace Src\Requests;

class AuthRequest extends BaseRequest
{
    /** @var string */
    protected $email;
    /** @var string */
    protected $password;

    public function __construct()
    {
        $this->email = $_POST['email'];
        $this->password = $_POST['password'];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function validate(): array
    {
        parent::validate();

        return [
            'email' => $this->email,
            'password' => $this->password,
        ];
    }

    protected function checkRules(): void
    {
        $this->baseStringCheck('email');
        $this->baseStringCheck('password');
    }
}