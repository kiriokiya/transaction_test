<?php

namespace Src\requests;

use Src\AuthUser;
use Src\Repositories\WalletRepository;

class WriteOffRequest extends BaseRequest
{
    public const MIN_WRITE_OFF_SUM = 0.01;
    public const MAX_WRITE_OFF_SUM_PRECISION = 2;

    /** @var int */
    protected $walletId;
    /** @var float */
    protected $sum;


    public function __construct()
    {
        $this->walletId = (int)$_POST['walletId'];
        $this->sum = $_POST['sum'];
    }

    public function validate(): array
    {
        parent::validate();

        return [
            'walletId' => $this->walletId,
            'sum' => $this->sum,
        ];
    }

    /**
     * @throws \Exception
     */
    protected function checkRules(): void
    {
        $this->baseIntegerCheck('walletId');
        $this->baseFloatCheck('sum');
        $this->checkWallet();
        $this->checkSum();
    }

    /**
     * @throws \Exception
     */
    private function checkWallet(): void
    {
        $repository = new WalletRepository();
        $repository->getWalletByIdAndUserId($this->walletId, AuthUser::get()->getId());
    }

    private function checkSum(): void
    {
        if ($this->sum < self::MIN_WRITE_OFF_SUM) {
            $this->errors[] = 'Минимальная сумма ' . self::MIN_WRITE_OFF_SUM;
        }

        if (strlen(substr(strrchr($this->sum, "."), 1)) > self::MAX_WRITE_OFF_SUM_PRECISION) {
            $this->errors[] = 'Максимальная точность после запятой - два символа';
        }
    }
}