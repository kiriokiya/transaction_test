<?php

namespace Src\Models;

class Transaction extends Model
{
    /** @var int */
    private $id;
    /** @var int */
    private $wallet_id;
    /** @var float */
    private $sum;
    /** @var string */
    private $date;

    /**
     * @return string
     */
    public static function getTableName(): string
    {
        return 'transactions';
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Transaction
     */
    public function setId(int $id): Transaction
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getWalletId(): int
    {
        return $this->wallet_id;
    }

    /**
     * @param int $wallet_id
     * @return Transaction
     */
    public function setWalletId(int $wallet_id): Transaction
    {
        $this->wallet_id = $wallet_id;

        return $this;
    }

    /**
     * @return float
     */
    public function getSum(): float
    {
        return $this->sum;
    }

    /**
     * @param float $sum
     * @return Transaction
     */
    public function setSum(float $sum): Transaction
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     * @return Transaction
     */
    public function setDate(string $date): Transaction
    {
        $this->date = $date;

        return $this;
    }


}