<?php

namespace Src\Models;

class Wallet extends Model
{
    /*** @var int */
    private $id;
    /*** @var int */
    private $user_id;
    /*** @var string */
    private $number;
    /*** @var float */
    private $balance;

    public static function getTableName(): string
    {
        return 'wallets';
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Wallet
     */
    public function setId(int $id): Wallet
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     * @return Wallet
     */
    public function setUserId(int $user_id): Wallet
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Wallet
     */
    public function setNumber(string $number): Wallet
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return $this->balance;
    }

    /**
     * @param float $balance
     * @return Wallet
     */
    public function setBalance(float $balance): Wallet
    {
        $this->balance = $balance;

        return $this;
    }
}