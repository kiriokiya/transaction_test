<?php

namespace Src;

use Src\Models\User;
use Src\Repositories\UserRepository;

class AuthUser
{
    /** @var AuthUser */
    private static $instance;
    /** @var User */
    public static $user;

    /**
     * DBConnection constructor.
     */
    private function __construct()
    {
        session_start();
        $email = $_SESSION['email'];
        session_write_close();
        if ($email) {
            $repository = new UserRepository();
            self::$user = $repository->getUserByEmail($email);
        } else {
            $server = $_SERVER['HTTP_HOST'];
            header("Location: http://$server/auth", true, 301);
        }
    }

    private function __clone()
    {

    }

    public static function get()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$user;
    }
}