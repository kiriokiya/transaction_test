<?php

namespace Src\Repositories;

use Src\Models\User;

class UserRepository extends BaseRepository
{
    protected function setTableName(): void
    {
        $this->tableName = User::getTableName();
    }

    /**
     * @param string $email
     * @return User
     * @throws \Exception
     */
    public function getUserByEmail(string $email): User
    {
        $query = $this->db->prepare("SELECT * FROM {$this->tableName} WHERE email = :email");
        $query->execute([':email' => $email]);

        $model = $query->fetchObject(User::class);

        $this->checkExist($model);

        return $model;
    }
}