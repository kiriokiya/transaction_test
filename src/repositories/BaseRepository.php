<?php

namespace Src\Repositories;

use Src\Connector;

abstract class BaseRepository
{
    /** @var \PDO */
    protected $db;
    /** @var string */
    protected $tableName;

    public function __construct()
    {
        $this->db = Connector::get();
        $this->setTableName();
    }

    /**
     * @param $result
     * @throws \Exception
     */
    protected function checkExist($result): void
    {
        if (!$result) {
            throw new \Exception('Модель не найдена');
        }
    }

    abstract protected function setTableName(): void;
}