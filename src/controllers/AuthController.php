<?php

namespace Src\Controllers;

use Src\Repositories\UserRepository;
use Src\Requests\AuthRequest;
use Src\Services\AuthService;

class AuthController
{
    public function getAuthForm(): void
    {
        include __DIR__ . '/../views/auth_form.php';
    }

    /**
     * @throws \Exception
     */
    public function auth(): void
    {
        $request = new AuthRequest();
        $data = $request->validate();

        $service = new AuthService(new UserRepository());
        $service->auth($data);
    }
}