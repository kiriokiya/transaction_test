<?php

namespace Src\Controllers;

use Src\Models\User;
use Src\Repositories\TransactionRepository;
use Src\Repositories\WalletRepository;
use Src\requests\WriteOffRequest;
use Src\Services\WalletService;
use Src\AuthUser;

class MainController
{
    /** @var User */
    private $user;

    public function __construct()
    {
        $this->user = AuthUser::get();
    }

    public function index(): void
    {
        $service = new WalletService(new WalletRepository(), new TransactionRepository());
        $wallets = $service->getWalletsByUserId($this->user->getId());

        include __DIR__ . '/../views/wallets.php';
    }

    public function writeOff(): void
    {
        $request = new WriteOffRequest();
        $data = $request->validate();

        $service = new WalletService(new WalletRepository(), new TransactionRepository());
        $service->writeOff($data['sum'], $data['walletId']);
    }
}
