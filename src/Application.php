<?php

namespace Src;

class Application
{
    /** @var array */
    private $routes;

    public function __construct()
    {
        Connector::get();
        $this->routes = include __DIR__ . '/../config/routes.php';
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function get()
    {
        [$controllerName, $action] = $this->getAction();
        $this->checkController($controllerName);
        $controller = new $controllerName();
        $this->checkAction($controller, $action);

        return $controller->$action();

    }

    protected function getAction(): array
    {
        $key = $_SERVER['REQUEST_METHOD'] . ' ' . $_SERVER['REQUEST_URI'];
        $controllerAction = explode('::', $this->routes[$key]);

        return ['Src\Controllers\\' . $controllerAction[0], $controllerAction[1]];
    }

    /**
     * @param $controllerName
     * @throws \Exception
     */
    private function checkController($controllerName): void
    {
        if (!class_exists($controllerName)) {
            throw new \Exception('Недоступное действие');
        }
    }

    /**
     * @param $controller
     * @param $action
     * @throws \Exception
     */
    private function checkAction($controller, $action): void
    {
        if (!method_exists($controller, $action)) {
            throw new \Exception('Недоступное действие');
        }
    }
}